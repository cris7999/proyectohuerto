package com.soprasteria.takeup.controladores;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.soprasteria.takeup.dto.MacetaDto;
import com.soprasteria.takeup.dto.PlantaMacetaDto;
import com.soprasteria.takeup.dto.TipoPlanta;
import com.soprasteria.takeup.interfaces.IPlantaServicio;

@RestController
@CrossOrigin(origins ="*", methods={RequestMethod.GET,RequestMethod.POST}) 
@RequestMapping("planta")
public class TipoPlantaController {

	@Autowired
	private IPlantaServicio ips;
	
	@Value("${url.maceta}")
	private String urlMaceta;
	
	@Value("${url.macetaplanta}")
	private String urlEntrega;
	
	@Value("${url.macetaeliminarplanta}")
	private String urlEliminarPlanta;
	
	//Planta
	@PostMapping("/nueva")
	public int nuevaPlanta(com.soprasteria.takeup.dto.TipoPlanta p) {
		com.soprasteria.takeup.modelos.TipoPlanta ep= new com.soprasteria.takeup.modelos.TipoPlanta();
		
		ep.setLitrosSustrato(p.getLitrosSustrato());
		ep.setNombre(p.getNombre());
		return ips.nuevaPlanta(ep);	
	}
	
	@GetMapping("/dep")
	public void dep() {
		System.out.println("Easter egg!");
		while(true) {
			System.out.println("Esto es todo amigos");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@DeleteMapping("/borrar/{nombrePlanta}")
	public int borrado(@PathVariable String nombrePlanta) {
		ips.borrar(ips.buscarIdPorNombre(nombrePlanta));
		return 1;
	}
	
	@GetMapping("/obtenerPlantas")
	public ArrayList<com.soprasteria.takeup.dto.TipoPlanta> obtenerPlantas(){
		return ips.obtenerPlantas();
	}
	
	//Compatibilidad
	@GetMapping("/compatibilidad/obtener/{planta1}/{planta2}")
	public int obtenerCompatibilidades(@PathVariable String planta1,@PathVariable String planta2){
		return ips.obtenerCompatibilidades(planta1,planta2);
	}
	
	@GetMapping("/prueba")
	public int test() {
		return 1;
	}
	
	//Plantacion
	@Transactional
	@PostMapping("/eliminar/{idPlanta}/{idMaceta}")
	public int desplantar(@PathVariable int idPlanta, @PathVariable int idMaceta){
		try {
			RestTemplate restTemplate = new RestTemplate();
			MacetaDto maceta=new MacetaDto();
			maceta.setId(idMaceta);
			com.soprasteria.takeup.dto.TipoPlanta tp=new com.soprasteria.takeup.dto.TipoPlanta();
			tp.setId(idPlanta);
			tp.setLitrosSustrato(ips.buscarLitrosPorId(tp.getId()));
			PlantaMacetaDto pm=new PlantaMacetaDto(maceta,tp);
			
			restTemplate.postForEntity(urlEliminarPlanta,pm,Integer.class);
			ips.eliminarPlantacion(idPlanta,idMaceta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}
	
	@GetMapping("/obtenerPlantaciones")
	public ArrayList<com.soprasteria.takeup.dto.Plantacion> obtenerPlantaciones(){
		return ips.obtenerPlantaciones();
	}
	
	@PostMapping("/plantar/{planta}/{idMaceta}/{idHuerto}")
	public int plantar(@PathVariable String planta,@PathVariable int idMaceta,@PathVariable int idHuerto) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			
			String url = urlMaceta;			
			MacetaDto macetaNueva= new MacetaDto();
			macetaNueva.setId(idMaceta);
			macetaNueva= restTemplate.postForObject(url,macetaNueva,MacetaDto.class);
			
			TipoPlanta nuevaPlanta= new TipoPlanta();
			nuevaPlanta.setNombre(planta);
			nuevaPlanta.setId(ips.buscarIdPorNombre(planta));
			nuevaPlanta.setLitrosSustrato(ips.buscarLitrosPorId(nuevaPlanta.getId()));
			
			//Validar
			if(macetaNueva.getCapacidadactual()>nuevaPlanta.getLitrosSustrato()) {
				ips.nuevaPlantacion(nuevaPlanta.getId(),macetaNueva.getId());
				restTemplate.postForObject(urlEntrega, new PlantaMacetaDto(macetaNueva,nuevaPlanta), Integer.class);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}
	@Transactional
	@DeleteMapping("/eliminarPlantasMaceta")
	public int eliminarPlantasMaceta(@RequestBody MacetaDto maceta) {
		try {
			ips.eliminarTodasPlantasDeMaceta(maceta.getId());						
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}
}
