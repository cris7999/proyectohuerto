package com.soprasteria.takeup.interfaces;

import java.util.ArrayList;

import com.soprasteria.takeup.dto.MacetaDto;
import com.soprasteria.takeup.modelos.TipoPlanta;

public interface IPlantaServicio {

	public int nuevaPlanta(TipoPlanta ep);
	public ArrayList<com.soprasteria.takeup.dto.TipoPlanta> obtenerPlantas();
	public int borrar(int id);
	public int buscarIdPorNombre(String nombre);
	public int buscarLitrosPorId(int id);
	public int eliminarPlantacion(int idPlanta,int idMaceta);
	public int nuevaPlantacion(int idPlanta,int idMaceta);
	public int obtenerCompatibilidades(String planta1, String planta2);
	public ArrayList<com.soprasteria.takeup.dto.Plantacion>obtenerPlantaciones();
	public int eliminarTodasPlantasDeMaceta(int planta);
	public MacetaDto obtenerMacetaPorPlanta(int planta);
}
