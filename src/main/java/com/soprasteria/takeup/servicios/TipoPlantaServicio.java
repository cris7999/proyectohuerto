package com.soprasteria.takeup.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soprasteria.takeup.dto.MacetaDto;
import com.soprasteria.takeup.interfaces.IPlantaServicio;
import com.soprasteria.takeup.modelos.Compatibilidad;
import com.soprasteria.takeup.modelos.CompatibilidadPK;
import com.soprasteria.takeup.modelos.Plantacion;
import com.soprasteria.takeup.modelos.TipoPlanta;
import com.soprasteria.takeup.repositorios.CompatibilidadRepository;
import com.soprasteria.takeup.repositorios.PlantaRepository;
import com.soprasteria.takeup.repositorios.PlantacionRepository;

@Service
public class TipoPlantaServicio implements IPlantaServicio {

	@Autowired
	PlantaRepository plantaRepository;
	
	@Autowired
	CompatibilidadRepository compaRepository;
	
	@Autowired
	PlantacionRepository plantacionRepository;
	
	public int buscarIdPorNombre(String nombre) {
		return plantaRepository.findByNombre(nombre).getId();
	}
	
	public int buscarLitrosPorId(int id) {
		return plantaRepository.findById(id).get().getLitrosSustrato();
	}
	 
	@Override
	@Transactional
	public int borrar(int id) {
		plantaRepository.deleteById(id);
		return 0;
	}
	@Override
	@Transactional
	public int nuevaPlanta(TipoPlanta ep) {
		System.out.println("Esto vamos a insertar:"+ep.getNombre()+" "+ep.getLitrosSustrato()+" "+ep.getImg());
		
		
		if(plantaRepository.findByNombre(ep.getNombre())==null) {
			plantaRepository.insertarPlanta(ep.getNombre(),ep.getLitrosSustrato(),ep.getImg());
			return 1;
		}else
			return 0;	
	}
	@Override
	public ArrayList<com.soprasteria.takeup.dto.TipoPlanta> obtenerPlantas() {
		ArrayList<com.soprasteria.takeup.dto.TipoPlanta> plantasDTO = new ArrayList<>();
		
		List<com.soprasteria.takeup.modelos.TipoPlanta> plantasEntity = plantaRepository.findAll();
		
		for(com.soprasteria.takeup.modelos.TipoPlanta misPlantas: plantasEntity) {
			com.soprasteria.takeup.dto.TipoPlanta nuevaDto=new com.soprasteria.takeup.dto.TipoPlanta();
			nuevaDto.setId(misPlantas.getId());
			nuevaDto.setLitrosSustrato(misPlantas.getLitrosSustrato());
			nuevaDto.setNombre(misPlantas.getNombre());
			plantasDTO.add(nuevaDto);
		}
		return plantasDTO;
	}

	@Override
	public int obtenerCompatibilidades(String planta1, String planta2) {
		if(planta1==null)
			planta1="";
		if(planta2==null)
			planta2="";
				com.soprasteria.takeup.modelos.TipoPlanta ep1 = plantaRepository.findByNombre(planta1);
		com.soprasteria.takeup.modelos.TipoPlanta ep2 = plantaRepository.findByNombre(planta2);
		if (ep1==null || ep2==null)
			return 0;
		System.out.println("mostrando: "+ep1.getCompatibilidads1());
		
		CompatibilidadPK objPk = new CompatibilidadPK ();
		objPk.setIdPlanta1(ep1.getId());
		objPk.setIdPlanta2(ep2.getId());
		
		Optional <Compatibilidad> obj =compaRepository.findById(objPk);
		
		if(obj.isPresent()) {
			return obj.get().getTipoCompatibilidad();
		}
		else return 0;
	}
	@Transactional
	@Override
	public int nuevaPlantacion(int idPlanta, int idMaceta) {
		
		plantacionRepository.insertarPlantacion(idPlanta, idMaceta);
		return 1;
	}
	@Transactional
	@Override
	public int eliminarPlantacion(int idPlanta, int idMaceta) {
		List<Plantacion> lp=plantacionRepository.findAll();
		if(!lp.isEmpty()) {
			plantacionRepository.eliminarPlantaYMaceta(idPlanta, idMaceta);
			return 1;
		}
		else
			return 0;
	}

	@Override
	public ArrayList<com.soprasteria.takeup.dto.Plantacion> obtenerPlantaciones() {
		 List<Plantacion> lista=plantacionRepository.findAll();
		 ArrayList<com.soprasteria.takeup.dto.Plantacion> plantaciones=new ArrayList<>();
		 com.soprasteria.takeup.dto.Plantacion plantacionNueva=new com.soprasteria.takeup.dto.Plantacion();
		 
		 for(Plantacion c:lista) {
			 plantacionNueva.setIdMaceta(c.getIdMaceta());
			 plantacionNueva.setIdPlanta(c.getTipoPlanta().getId());
			 plantaciones.add(plantacionNueva);
		 }
		 return plantaciones;

	}

	@Override
	public int eliminarTodasPlantasDeMaceta(int idMaceta) {
		plantacionRepository.eliminarPlantacion(idMaceta);
		return 1;
	}

	@Override
	public MacetaDto obtenerMacetaPorPlanta(int planta) {
		
		Plantacion p=plantacionRepository.obtenerMacetaPorIdPlanta(planta);
		MacetaDto m=new MacetaDto();
		m.setId(p.getIdMaceta());
		return m;
	}

	
	

}
