package com.soprasteria.takeup.dto;

public class PlantaMacetaDto {
	private MacetaDto m;
	private TipoPlanta tp;
	
	public PlantaMacetaDto(MacetaDto m,TipoPlanta tp) {
		this.m=m;
		this.tp=tp;
	}
	public MacetaDto getM() {
		return m;
	}
	public void setM(MacetaDto m) {
		this.m = m;
	}
	public TipoPlanta getTp() {
		return tp;
	}
	public void setTp(TipoPlanta tp) {
		this.tp = tp;
	}
	
}
