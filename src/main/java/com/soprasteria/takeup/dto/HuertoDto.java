package com.soprasteria.takeup.dto;

public class HuertoDto {
	private int id;
	private String nombre;
	private int nMacetas;
	private int nPlantas;
	private int capacidadMaxima;
	private int capacidadActual;

	public HuertoDto(int id, String nombre, int nMacetas, int nPlantas,  int capacidadMaxima, int capacidadActual) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.nMacetas = nMacetas;
		this.nPlantas = nPlantas;
		this.capacidadMaxima = capacidadMaxima;
		this.capacidadActual = capacidadActual;
	}
	
	public HuertoDto(int id) {
		super();
		this.id = id;
	}

	public HuertoDto() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getnPlantas() {
		return nPlantas;
	}

	public void setnPlantas(int nPlantas) {
		this.nPlantas = nPlantas;
	}

	public int getnMacetas() {
		return nMacetas;
	}

	public void setnMacetas(int nMacetas) {
		this.nMacetas = nMacetas;
	}

	public int getCapacidadMaxima() {
		return capacidadMaxima;
	}

	public void setCapacidadMaxima(int capacidadMaxima) {
		this.capacidadMaxima = capacidadMaxima;
	}

	public int getCapacidadActual() {
		return capacidadActual;
	}

	public void setCapacidadActual(int capacidadActual) {
		this.capacidadActual = capacidadActual;
	}

}
