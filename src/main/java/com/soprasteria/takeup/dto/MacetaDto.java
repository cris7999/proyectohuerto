package com.soprasteria.takeup.dto;

public class MacetaDto {

	private int id;
	private int capacidadmaxima;
	private int capacidadactual;
	private int idhuerto;
	private int numplantas;
	
	public MacetaDto() {
		
	}
	
	public MacetaDto(int id, int capacidadmaxima, int capacidadactual, int idhuerto, int numplantas) {
		super();
		this.id = id;
		this.capacidadmaxima = capacidadmaxima;
		this.capacidadactual = capacidadactual;
		this.idhuerto = idhuerto;
		this.numplantas = numplantas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCapacidadmaxima() {
		return capacidadmaxima;
	}

	public void setCapacidadmaxima(int capacidadmaxima) {
		this.capacidadmaxima = capacidadmaxima;
	}

	public int getCapacidadactual() {
		return capacidadactual;
	}

	public void setCapacidadactual(int capacidadactual) {
		this.capacidadactual = capacidadactual;
	}

	public int getIdhuerto() {
		return idhuerto;
	}

	public void setIdhuerto(int idhuerto) {
		this.idhuerto = idhuerto;
	}

	public int getNumplantas() {
		return numplantas;
	}

	public void setNumplantas(int numplantas) {
		this.numplantas = numplantas;
	}

	
}
