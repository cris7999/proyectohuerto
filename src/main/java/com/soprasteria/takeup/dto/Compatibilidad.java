package com.soprasteria.takeup.dto;

public class Compatibilidad {
	int idPlanta1;
	int idPlanta2;
	int tipoCompatibilidad;
	public int getIdPlanta1() {
		return idPlanta1;
	}
	public void setIdPlanta1(int idPlanta1) {
		this.idPlanta1 = idPlanta1;
	}
	public int getIdPlanta2() {
		return idPlanta2;
	}
	public void setIdPlanta2(int idPlanta2) {
		this.idPlanta2 = idPlanta2;
	}
	public int getTipoCompatibilidad() {
		return tipoCompatibilidad;
	}
	public void setTipoCompatibilidad(int tipoCompatibilidad) {
		this.tipoCompatibilidad = tipoCompatibilidad;
	}
}