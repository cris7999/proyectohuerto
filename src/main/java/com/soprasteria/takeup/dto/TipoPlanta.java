package com.soprasteria.takeup.dto;

public class TipoPlanta {
	private int id;
	private String nombre;
	private int litrosSustrato;

	public TipoPlanta() {}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getLitrosSustrato() {
		return litrosSustrato;
	}
	public void setLitrosSustrato(int litrosSustrato) {
		this.litrosSustrato = litrosSustrato;
	}
}
