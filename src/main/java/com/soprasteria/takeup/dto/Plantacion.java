package com.soprasteria.takeup.dto;



public class Plantacion {
	private int idPlanta;
	private int idMaceta;
	public int getIdPlanta() {
		return idPlanta;
	}
	public void setIdPlanta(int idPlanta) {
		this.idPlanta = idPlanta;
	}
	public int getIdMaceta() {
		return idMaceta;
	}
	public void setIdMaceta(int idMaceta) {
		this.idMaceta = idMaceta;
	}
}
