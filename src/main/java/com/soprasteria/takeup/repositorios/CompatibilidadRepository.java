package com.soprasteria.takeup.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soprasteria.takeup.modelos.Compatibilidad;
import com.soprasteria.takeup.modelos.CompatibilidadPK;

public interface CompatibilidadRepository extends JpaRepository<Compatibilidad,CompatibilidadPK>{

	
	
}
