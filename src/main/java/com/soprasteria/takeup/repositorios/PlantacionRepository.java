package com.soprasteria.takeup.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.soprasteria.takeup.modelos.Plantacion;

public interface PlantacionRepository extends JpaRepository<Plantacion,Integer>{

	@Modifying
	@Query(value = "insert into plantacion(id_plantacion,id_planta,id_maceta) values (PLANTA_SEC.nextVal,?1,?2)", nativeQuery = true)
	void insertarPlantacion(int idPlanta, int idMaceta);
		
	@Modifying
	@Query(value = "delete from plantacion where id_maceta=?1", nativeQuery = true)
	void eliminarPlantacion(int idMaceta);
	
	@Query (value="select p from Plantacion p where p.tipoPlanta.id=?1")
	Plantacion obtenerMacetaPorIdPlanta (int idPlanta);
	
	@Modifying
	@Query(value="delete from Plantacion p where p.tipoPlanta.id=?1 and idMaceta=?2")
	void eliminarPlantaYMaceta(int idPlanta,int idMaceta);
	
}