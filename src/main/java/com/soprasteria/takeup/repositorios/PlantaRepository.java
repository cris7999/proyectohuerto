package com.soprasteria.takeup.repositorios;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.soprasteria.takeup.modelos.TipoPlanta;

public interface PlantaRepository extends JpaRepository<TipoPlanta,Integer>{
	List<TipoPlanta> findAll();
		
	@Query(value = "SELECT * FROM Tipo_Planta WHERE nombre = ?1", nativeQuery = true)
	TipoPlanta findByNombre(String nombre);
	 
	@Modifying
	@Query(value = "insert into tipo_planta values (PLANTA_SEC.nextVal,?1,?2,?3)", nativeQuery = true)
	void insertarPlanta(String nombre, int litrosSustrato,String imagen);

}
