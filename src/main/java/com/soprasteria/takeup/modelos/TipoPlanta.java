package com.soprasteria.takeup.modelos;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TIPO_PLANTA database table.
 * 
 */
@Entity
@Table(name="TIPO_PLANTA")
@NamedQuery(name="TipoPlanta.findAll", query="SELECT t FROM TipoPlanta t")
public class TipoPlanta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String img;

	@Column(name="LITROS_SUSTRATO")
	private int litrosSustrato;

	private String nombre;

	//bi-directional many-to-one association to Compatibilidad
	@OneToMany(mappedBy="tipoPlanta1")
	private List<Compatibilidad> compatibilidads1;

	//bi-directional many-to-one association to Compatibilidad
	@OneToMany(mappedBy="tipoPlanta2")
	private List<Compatibilidad> compatibilidads2;

	public TipoPlanta() {
	}

	public int getId() {
		return this.id;
	}
 
	public void setId(int id) {
		this.id = id;
	}

	public String getImg() {
		return this.img;
	}
 
	public void setImg(String img) {
		this.img = img;
	}

	public int getLitrosSustrato() {
		return this.litrosSustrato;
	}

	public void setLitrosSustrato(int litrosSustrato) {
		this.litrosSustrato = litrosSustrato;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Compatibilidad> getCompatibilidads1() {
		return this.compatibilidads1;
	}

	public void setCompatibilidads1(List<Compatibilidad> compatibilidads1) {
		this.compatibilidads1 = compatibilidads1;
	}

	public Compatibilidad addCompatibilidads1(Compatibilidad compatibilidads1) {
		getCompatibilidads1().add(compatibilidads1);
		compatibilidads1.setTipoPlanta1(this);

		return compatibilidads1;
	}

	public Compatibilidad removeCompatibilidads1(Compatibilidad compatibilidads1) {
		getCompatibilidads1().remove(compatibilidads1);
		compatibilidads1.setTipoPlanta1(null);

		return compatibilidads1;
	}

	public List<Compatibilidad> getCompatibilidads2() {
		return this.compatibilidads2;
	}

	public void setCompatibilidads2(List<Compatibilidad> compatibilidads2) {
		this.compatibilidads2 = compatibilidads2;
	}

	public Compatibilidad addCompatibilidads2(Compatibilidad compatibilidads2) {
		getCompatibilidads2().add(compatibilidads2);
		compatibilidads2.setTipoPlanta2(this);

		return compatibilidads2;
	}

	public Compatibilidad removeCompatibilidads2(Compatibilidad compatibilidads2) {
		getCompatibilidads2().remove(compatibilidads2);
		compatibilidads2.setTipoPlanta2(null);

		return compatibilidads2;
	}


}