package com.soprasteria.takeup.modelos;
import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the COMPATIBILIDAD database table.
 * 
 */
@Entity
@NamedQuery(name="Compatibilidad.findAll", query="SELECT c FROM Compatibilidad c")
public class Compatibilidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CompatibilidadPK id;

	@Column(name="TIPO_COMPATIBILIDAD")
	private int tipoCompatibilidad;

	//bi-directional many-to-one association to TipoPlanta
	@ManyToOne
	@JoinColumn(name="ID_PLANTA1",insertable = false, updatable = false) 
	private TipoPlanta tipoPlanta1;

	//bi-directional many-to-one association to TipoPlanta
	@ManyToOne
	@JoinColumn(name="ID_PLANTA2",insertable = false, updatable = false)
	private TipoPlanta tipoPlanta2;

	public Compatibilidad() {
	}

	public CompatibilidadPK getId() {
		return this.id;
	}

	public void setId(CompatibilidadPK id) {
		this.id = id;
	}

	public int getTipoCompatibilidad() {
		return this.tipoCompatibilidad;
	}

	public void setTipoCompatibilidad(int tipoCompatibilidad) {
		this.tipoCompatibilidad = tipoCompatibilidad;
	}

	public TipoPlanta getTipoPlanta1() {
		return this.tipoPlanta1;
	}

	public void setTipoPlanta1(TipoPlanta tipoPlanta1) {
		this.tipoPlanta1 = tipoPlanta1;
	}

	public TipoPlanta getTipoPlanta2() {
		return this.tipoPlanta2;
	}

	public void setTipoPlanta2(TipoPlanta tipoPlanta2) {
		this.tipoPlanta2 = tipoPlanta2;
	}

}