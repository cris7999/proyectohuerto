package com.soprasteria.takeup.modelos;

import java.io.Serializable;
import javax.persistence.*;
/**
 * The persistent class for the PLANTACION database table.
 * 
 */
@Entity
@NamedQuery(name="Plantacion.findAll", query="SELECT p FROM Plantacion p")
public class Plantacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PLANTACION")
	private int idPlantacion;

	@Column(name="ID_MACETA")
	private int idMaceta;

	//bi-directional many-to-one association to TipoPlanta
	@ManyToOne
	@JoinColumn(name="ID_PLANTA")
	private TipoPlanta tipoPlanta;

	public Plantacion() {
	}

	public long getIdPlantacion() {
		return this.idPlantacion;
	}

	public void setIdPlantacion(int idPlantacion) {
		this.idPlantacion = idPlantacion;
	}

	public int getIdMaceta() {
		return this.idMaceta;
	}

	public void setIdMaceta(int idMaceta) {
		this.idMaceta = idMaceta;
	}

	public TipoPlanta getTipoPlanta() {
		return this.tipoPlanta;
	}

	public void setTipoPlanta(TipoPlanta tipoPlanta) {
		this.tipoPlanta = tipoPlanta;
	}

}