package com.soprasteria.takeup.modelos;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the COMPATIBILIDAD database table.
 * 
 */
@Embeddable
public class CompatibilidadPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_PLANTA1", insertable=false, updatable=false)
	private long idPlanta1;

	@Column(name="ID_PLANTA2", insertable=false, updatable=false)
	private long idPlanta2;

	public CompatibilidadPK() {
	}
	public long getIdPlanta1() {
		return this.idPlanta1;
	}
	public void setIdPlanta1(long idPlanta1) {
		this.idPlanta1 = idPlanta1;
	}
	public long getIdPlanta2() {
		return this.idPlanta2;
	}
	public void setIdPlanta2(long idPlanta2) {
		this.idPlanta2 = idPlanta2;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CompatibilidadPK)) {
			return false;
		}
		CompatibilidadPK castOther = (CompatibilidadPK)other;
		return 
			(this.idPlanta1 == castOther.idPlanta1)
			&& (this.idPlanta2 == castOther.idPlanta2);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idPlanta1 ^ (this.idPlanta1 >>> 32)));
		hash = hash * prime + ((int) (this.idPlanta2 ^ (this.idPlanta2 >>> 32)));
		
		return hash;
	}
}